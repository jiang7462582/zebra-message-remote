name := """zebra-message-remote"""

version := "1.2"

organization := "cn.bmkp"

scalaVersion := "2.11.11"

libraryDependencies ++= Seq(
  "junit" % "junit" % "4.12" % "test",
  "com.novocode" % "junit-interface" % "0.11" % "test")

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % "2.4.12",
  "com.typesafe.akka" %% "akka-stream" % "2.4.12",
  "com.typesafe.akka" %% "akka-testkit" % "2.4.12" % Test,
  "com.typesafe.akka" % "akka-remote_2.11" % "2.4.12"
)

libraryDependencies += "com.typesafe.akka" % "akka-http_2.11" % "10.0.9"

libraryDependencies += "com.typesafe.play" % "play-json_2.11" % "2.5.15"

libraryDependencies += "com.typesafe.akka" %% "akka-stream-kafka" % "0.16"

// https://mvnrepository.com/artifact/com.github.levkhomich/akka-tracing-core_2.11
libraryDependencies += "com.github.levkhomich" % "akka-tracing-core_2.11" % "0.6"


mappings in (Compile, packageBin) ~= { _.filterNot { case (_, name) =>
  Seq("application.conf").contains(name)
}}