package com.zebrarun.push;

import java.io.Serializable;

/**
 * Created by jiang on 2017/7/17.
 */
public class pushActorVO {

    public static class CustomerPush implements Serializable{
        public String uid;
        public String tag;
        public String msg;
        public String onlyOnline = "0";
        public CustomerPush(String uid, String tag, String msg, String onlyOnline) {
            this.uid = uid;
            this.tag = tag;
            this.msg = msg;
            this.onlyOnline = onlyOnline;
        }
    }

    public static class DriverPush implements Serializable{
        public String uid;
        public String tag;
        public String msg;
        public String onlyOnline = "0";
        public DriverPush(String uid, String tag, String msg, String onlyOnline) {
            this.uid = uid;
            this.tag = tag;
            this.msg = msg;
            this.onlyOnline = onlyOnline;
        }

    }

}
