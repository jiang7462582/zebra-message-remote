package com.zebrarun.push;

import akka.actor.AbstractActor;
import akka.actor.Status;
import akka.japi.pf.ReceiveBuilder;
import com.zebrarun.sms.smsActorVO;
import com.zebrarun.utils.PushUtil;
import com.zebrarun.utils.RXLMsgUtil;
import scala.PartialFunction;
import scala.runtime.BoxedUnit;

/**
 * Created by jiang on 2017/7/17.
 */
public class pushActor extends AbstractActor {

    @Override
    public void preStart() {
        System.out.println("哈哈.我活了");
    }

    @Override
    public void postStop() {
        System.out.println("啊啊.我死了");
    }

    public PartialFunction<Object,BoxedUnit> receive() {
        return ReceiveBuilder.
                match(pushActorVO.CustomerPush.class, m->
                        sender().tell(PushUtil.sendCustomer(context(),m.uid,m.tag,m.msg),self())).
                matchAny(x ->
                        sender().tell(
                                new Status.Failure(new Exception("unknown message")), self()
                        )).
                build();
    }
}
