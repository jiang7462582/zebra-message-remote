package com.zebrarun.utils;

import akka.actor.ActorContext;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.dispatch.ExecutionContexts;
import akka.http.javadsl.Http;
import akka.http.javadsl.model.HttpRequest;
import akka.http.javadsl.model.HttpResponse;
import akka.http.javadsl.unmarshalling.Unmarshaller;
import akka.stream.ActorMaterializer;
import akka.stream.Materializer;
import com.fasterxml.jackson.databind.JsonNode;
import play.libs.Json;
import play.libs.Json.*;


import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;

/**
 * Created by jiang on 2017/7/11.
 */
public class RXLMsgUtil {

    private final static String BASE_URL = "http://apis.renxinl.com:8080/";
    private final static String userName = "18696186780";
    private final static String pwd = "jjf19901124";
    private final static String VERIFICATION_CODE_MID = "1";

    /**
     * @param phone 发送用户验证码短信接口
     * @param msg   发送短信内容
     * @param sendtime 可选 ,定时发送时间(空为立即发送 yyyy-MM-dd HH:mm:ss )
     * @return
     */

    public static String sendVerificationCode(ActorContext context,List<String> phone, String msg, String sendtime)throws UnsupportedEncodingException {
        ActorSystem system = context.system();
        Materializer materializer = ActorMaterializer.create(system);
        StringBuffer sb = new StringBuffer(BASE_URL+"smsgate/varsend.do?");
        sb.append("user=").append(userName).append("&");
        sb.append("pwd=").append(pwd).append("&");
        sb.append("params=").append(URLEncoder.encode(wrapUserParams(phone,msg),"UTF-8")).append("&");
        sb.append("mid=").append("2").append("&");
        sb.append("extno=").append("").append("&");
        sb.append("sendtime=").append(sendtime);

        String url = sb.toString();

        final CompletionStage<HttpResponse> responseFuture =
                Http.get(system)
                        .singleRequest(HttpRequest.create(url)
                                , materializer);
        return responseFuture.thenCompose(response -> Unmarshaller.entityToString().unmarshal(response.entity(), ExecutionContexts.global(), materializer))
                .toCompletableFuture().exceptionally(p->"错误信息").join();
    }

    public static String wrapUserParams(List<String> phones,String msg){
        return phones.stream().map(p->p+","+msg).collect(Collectors.joining(";"));
    }

}
