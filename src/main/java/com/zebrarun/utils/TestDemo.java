package com.zebrarun.utils;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.dispatch.ExecutionContexts;
import akka.http.javadsl.Http;
import akka.http.javadsl.marshalling.Marshaller;
import akka.http.javadsl.model.*;
import akka.http.javadsl.model.headers.Accept;
import akka.http.javadsl.model.headers.RawHeader;
import akka.http.javadsl.unmarshalling.Unmarshaller;
import akka.pattern.Patterns;
import akka.stream.ActorMaterializer;
import akka.stream.Materializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.zebrarun.sms.smsActor;
import play.libs.Json;
import scala.compat.java8.FutureConverters;

import java.util.concurrent.CompletionStage;

/**
 * Created by jiang on 2017/7/12.
 */
public class TestDemo {
    final static ActorSystem system = ActorSystem.create();
    final static Materializer materializer = ActorMaterializer.create(system);


    public static void test(){
        final CompletionStage<HttpResponse> responseFuture =
                Http.get(system)
                        .singleRequest(HttpRequest.create("http://www.renxinl.com:9999/rxl2016/ivrvoicecode.do?user=18696186780&pwd=1111&phone=18696186780&msg=123456")
                                , materializer);
        responseFuture.thenCompose(response -> Unmarshaller.entityToString().unmarshal(response.entity(), ExecutionContexts.global(), materializer))
                .thenAccept(System.out::println);



        HttpRequest httpRequest = HttpRequest.POST("https://apit.bmkp.cn/backend/v400/enterprise/findTagsByCity").addHeader(RawHeader.create("Authorization","bearer 3d4bbdea-ee93-4f41-8c1b-81c4e10f1b03"))
                .addHeader(Accept.create(MediaRanges.create(MediaTypes.APPLICATION_JSON)))
                .withEntity(MediaTypes.APPLICATION_JSON.toContentType(), "{\"city\": \"武汉市\"}");

        final CompletionStage<HttpResponse> postFuture =
                Http.get(system).singleRequest(httpRequest,materializer);
        postFuture.thenCompose(response -> Unmarshaller.entityToString().unmarshal(response.entity(), ExecutionContexts.global(), materializer))
                .thenApply(Json::parse).thenAccept(System.out::println);
    }
}
