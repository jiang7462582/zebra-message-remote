package com.zebrarun.utils;

import akka.actor.ActorContext;
import akka.actor.ActorSystem;
import akka.dispatch.ExecutionContexts;
import akka.http.javadsl.Http;
import akka.http.javadsl.model.*;
import akka.http.javadsl.model.headers.Accept;
import akka.http.javadsl.model.headers.RawHeader;
import akka.http.javadsl.unmarshalling.Unmarshaller;
import akka.stream.ActorMaterializer;
import akka.stream.Materializer;
import play.libs.Json;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletionStage;

/**
 * Created by jiang on 2017/7/17.
 */
public class PushUtil {

    public static String AppID_TEST = "203253";
    public static String SecretID_TEST = "AKIDJZeYGnsHF1bUFZlneJrDNL9lBPRA6N9p";
    public static String SecretKey_TEST = "fFgvypW1LWWKwSp23BAxf8VhhVAduzvl";

    public static String WnsURI = "http://wns.api.qcloud.com/api/";

    //TAG区分业务类型

    public static String TAG_SERVICE_TEST = "testMsg";

    private static String sign(String appId,String secretKey,Long timestamp){
        try {
            String plaintext = appId+"&"+timestamp;
            byte[] bytes = RSA.hmacSHA1(secretKey,plaintext);
            return URLEncoder.encode(Base64.encode(bytes),"utf-8");
        }catch (InvalidKeyException | NoSuchAlgorithmException | UnsupportedEncodingException e){
            return null;
        }
    }
    private static Map<String,String> signParas(String appId, String secretID, String secretKey) {
        Map<String,String> map = new HashMap<>();
        Long timestamp = System.currentTimeMillis()/1000L;
        String sign = sign(appId,secretKey,timestamp);
        if(sign == null )
            return null;
        map.put("tm",timestamp.toString());
        map.put("sign",sign);
        map.put("appId",appId);
        map.put("secretKey",secretKey);
        map.put("secretID",secretID);
        return map;
    }


    public static String sendMessage(ActorContext context, Map<String,String> signParas, String uid, String tag, String msg, String onlyOnline){
        ActorSystem system = context.system();
        Materializer materializer = ActorMaterializer.create(system);
        String sign = signParas.get("sign");
        String appId = signParas.get("appId");
        String secretID = signParas.get("secretID");
        String tm = signParas.get("tm");
        StringBuffer sb = new StringBuffer("");
        sb.append("appid=").append(appId).append("&");
        sb.append("secretid=").append(secretID).append("&");
        sb.append("sign=").append(sign).append("&");
        sb.append("uid=").append(uid).append("&");
        sb.append("tag=").append(tag).append("&");
        sb.append("tm=").append(tm).append("&");
        sb.append("only_online=").append(onlyOnline).append("&");
        sb.append("wid=").append("").append("&");
        sb.append("content=").append(msg);

        HttpRequest httpRequest = HttpRequest.POST(WnsURI+"send_msg_new")
                .withEntity(MediaTypes.APPLICATION_X_WWW_FORM_URLENCODED.toContentType(HttpCharsets.UTF_8), sb.toString());
        final CompletionStage<HttpResponse> postFuture =
                Http.get(system).singleRequest(httpRequest,materializer);
        return postFuture.thenCompose(response -> Unmarshaller.entityToString().unmarshal(response.entity(), ExecutionContexts.global(), materializer))
                .thenApply(Object::toString).toCompletableFuture().exceptionally(p->"错误信息").join();
    }

    public static String sendCustomer(ActorContext context,String uid,String tag,String msg){
        Map<String,String> mapParas = signParas(AppID_TEST,SecretID_TEST,SecretKey_TEST);
        if(mapParas == null)
            return null;
        return sendMessage(context,mapParas,uid,tag,msg,"0");
    }

}
