package com.zebrarun;

import akka.Done;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.dispatch.ExecutionContexts;
import akka.http.javadsl.Http;
import akka.http.javadsl.marshalling.Marshaller;
import akka.http.javadsl.model.*;
import akka.http.javadsl.model.headers.Accept;
import akka.http.javadsl.model.headers.RawHeader;
import akka.http.javadsl.unmarshalling.Unmarshaller;
import akka.kafka.ConsumerSettings;
import akka.pattern.CircuitBreaker;
import akka.pattern.Patterns;
import akka.stream.ActorMaterializer;
import akka.stream.Materializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StringDeserializer;
import com.zebrarun.kafka.KafkaStreamTest;
import com.zebrarun.push.pushActor;
import com.zebrarun.sms.smsActor;
import com.zebrarun.sms.smsActorVO;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.ByteArrayDeserializer;
import play.libs.Json;
import scala.compat.java8.FutureConverters;
import scala.concurrent.duration.FiniteDuration;


import java.util.Arrays;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

/**
 * Created by jiang on 2017/7/11.
 */
public class Main {

    public static void main(String[] args) {
        final ActorSystem system = ActorSystem.create("message");
        //new KafkaStreamTest(system);
        ActorRef actorRef = system.actorOf(Props.create(smsActor.class), "sms");
        ActorRef actorPush = system.actorOf(Props.create(pushActor.class).withDispatcher("push-dispatcher"), "push");
    }
}
