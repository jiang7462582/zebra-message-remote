package com.zebrarun.kafka;

import akka.Done;
import akka.actor.ActorSystem;
import akka.kafka.ConsumerSettings;

import akka.kafka.Subscriptions;
import akka.kafka.javadsl.Consumer;
import akka.stream.ActorMaterializer;
import akka.stream.javadsl.Sink;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.ByteArrayDeserializer;
import org.apache.kafka.common.serialization.StringDeserializer;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

/**
 * Created by jiang on 2017/7/14.
 */

public class KafkaStreamTest{
    private ActorSystem system;

    public KafkaStreamTest(ActorSystem system) {
        this.system = system;
        consumerDateReceive();
    }


    static class DB {
        public CompletionStage<Done> update(String data) {
            System.out.println("DB.update: " + data);
            return CompletableFuture.completedFuture(Done.getInstance());
        }
    }

    public  void consumerDateReceive(){
        ConsumerSettings<byte[], String> consumerSettings =
                ConsumerSettings.create(system, new ByteArrayDeserializer(), new org.apache.kafka.common.serialization.StringDeserializer())
                        .withBootstrapServers("139.199.12.28:19092")
                        .withGroupId("group1")
                        .withProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "latest");


        final DB db = new DB();

        Consumer.committableSource(consumerSettings, Subscriptions.topics("gps_jsydw"))
                .mapAsync(1, msg -> db.update(msg.record().value()).thenApply(done -> msg))
                .runWith(Sink.ignore(), ActorMaterializer.create(system));
    }



}
