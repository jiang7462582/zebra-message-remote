package com.zebrarun.sms;

import akka.actor.AbstractActor;
import akka.actor.Status;
import akka.japi.pf.ReceiveBuilder;
import com.zebrarun.utils.RXLMsgUtil;
import scala.PartialFunction;
import scala.runtime.BoxedUnit;

import java.util.List;

/**
 * Created by jiang on 2017/7/11.
 */
public class smsActor extends AbstractActor {

    @Override
    public void preStart() {
        System.out.println("哈哈.我启动了");
    }

    @Override
    public void postStop() {
        System.out.println("啦啦.我停止了");
    }

    public PartialFunction<Object,BoxedUnit> receive() {
        return ReceiveBuilder.
                match(smsActorVO.VerificationCode.class,m->
                        sender().tell(RXLMsgUtil.sendVerificationCode(context(),m.phones,m.msg,m.sendtime),self())).
                matchEquals("Ping", s ->
                        System.out.println("receiverMsg: "+s)).
                matchEquals("Stop", s ->
                        context().stop(self())).
                matchAny(x ->
                        sender().tell(
                                new Status.Failure(new Exception("unknown message")), self()
                        )).
                build();
    }
}
