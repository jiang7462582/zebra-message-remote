package com.zebrarun.sms;

import java.io.Serializable;
import java.util.List;

/**
 * Created by jiang on 2017/7/12.
 */


public class smsActorVO {
    //一定要继承Serializable
    public static class VerificationCode implements Serializable{
        public List<String> phones;  //批量发送用户手机号
        public String msg ;     // 发送短信内容
        public String sendtime ; //可选 ,定时发送时间(空为立即发送)

        public VerificationCode(List<String> phones, String msg, String sendtime) {
            this.phones = phones;
            this.msg = msg;
            this.sendtime = sendtime;
        }
    }
}
